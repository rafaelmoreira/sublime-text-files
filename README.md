Sublime Text 2 Support Files
============================

<pre>
  rm -rf ~/Library/Application\ Support/Sublime\ Text\ 2
  git clone git://github.com/rafamoreira/sublime-text-files.git ~/Library/Application\ Support/Sublime\ Text\ 2
  cd ~/Library/Application\ Support/Sublime\ Text\ 2
  git submodule update --init
</pre>